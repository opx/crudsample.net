﻿' addon function untuk modifikasi grid bawaan vs.net 

Namespace Komponen

    Public Enum enumColGridType
        egtString
        egtNumeric
        egtLookup
    End Enum

    Public Module modGridView

        Private Function GetFontPx(ByVal pFont As Font) As Integer
            Dim sz As Size
            sz = TextRenderer.MeasureText("X", pFont)
            'Return (sz.Width)
            Return 8
        End Function

        Sub GV_SetDefaultStyle(ByVal pGrid As DataGridView)
            With pGrid
                .Font = New Font("Consolas", 8)
                .ColumnHeadersDefaultCellStyle.Font = New Font(pGrid.Font, FontStyle.Bold)
                .MultiSelect = False
            End With
        End Sub

        Function GV_AddColStr(ByVal pGrid As DataGridView, ByVal pHeader As String, ByVal pField As String, ByVal pCharWidth As Integer) As DataGridViewColumn
            Dim xWidth As Integer = 10
            xWidth = pGrid.Font.Size * pCharWidth
            Dim oCol As New DataGridViewTextBoxColumn
            With oCol
                .HeaderText = UCase(pHeader)
                .Name = pField
                .DataPropertyName = pField
                .Width = xWidth
            End With
            pGrid.Columns.Add(oCol)
        End Function

        Function GV_AddColNum(ByVal pGrid As DataGridView, ByVal pHeader As String, ByVal pField As String) As DataGridViewColumn

        End Function

    End Module

End Namespace