﻿Imports System.ComponentModel
Imports CRS.Common.modTipe
Imports CRS.DataModel
Public Class frmBarang

    Private mSaveMode As enumSaveMode

    Private Sub ResetForm()
        ' fill combobox
        Dim oKat As New clsKategori
        With cboKategori
            .DataSource = oKat.DT_ListAktif()
            .DisplayMember = "NamaKategori"
            .ValueMember = "KodeKategori"
        End With
        oKat = Nothing

        mSaveMode = enumSaveMode.esmNew
        txtKode.Text = ""
        Call txtKode_Validated(Nothing, Nothing)
    End Sub

    Private Sub frmBarang_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ResetForm()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Dispose()
    End Sub

    Private Sub txtKode_Validated(sender As Object, e As EventArgs) Handles txtKode.Validated
        Dim oTrs As New clsBarang

        With oTrs
            .GetData(txtKode.Text)
            ' set save mode
            If .IsExists Then
                mSaveMode = enumSaveMode.esmUpdate
                txtKode.Text = .KodeBarang

                btnDelete.Enabled = True
            Else
                mSaveMode = enumSaveMode.esmNew

                btnDelete.Enabled = False
            End If

            ' load data
            txtNama.Text = .NamaBarang
            cboKategori.SelectedValue = .KodeKategori
            chkAktif.Checked = .Aktif
        End With

        oTrs = Nothing
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim oTrs As New clsBarang

        With oTrs
            .KodeBarang = txtKode.Text
            .NamaBarang = txtNama.Text
            .KodeKategori = cboKategori.SelectedValue
            .Aktif = chkAktif.Checked
        End With
        If Not oTrs.Save(mSaveMode) Then
            MsgBox(oTrs.LastMsg, MsgBoxStyle.Exclamation, "Error")
        Else
            MsgBox("Data tersimpan", MsgBoxStyle.Information, "Info")
            ResetForm()
            txtKode.Focus()
        End If

        oTrs = Nothing
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If MsgBox("Hapus data " & txtKode.Text & " - " & txtNama.Text & " ?", MsgBoxStyle.Question & vbYesNo, "Konfirmasi") = MsgBoxResult.Yes Then
            Dim oTrs As New clsBarang
            If Not oTrs.Delete(txtKode.Text) Then
                MsgBox(oTrs.LastMsg, MsgBoxStyle.Exclamation, "Error")
            Else
                MsgBox("Data telah dihapus", MsgBoxStyle.Information, "Info")
                ResetForm()
                txtKode.Focus()
            End If
            oTrs = Nothing
        End If
    End Sub

    Private Sub btnLookup_Click(sender As Object, e As EventArgs) Handles btnLookup.Click
        Dim oTrs As New clsBarang
        Dim xData As String = ""

        xData = CStr(oTrs.LookupAll())
        If Trim(xData) <> "" Then
            txtKode.Text = xData
            SendKeys.Send("{TAB}")
            Call txtKode_Validated(Nothing, Nothing)
        End If

        oTrs = Nothing
    End Sub

End Class