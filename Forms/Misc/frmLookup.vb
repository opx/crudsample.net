﻿Imports CRS.Komponen.modGridView
Imports CRS.Utility

Public Class frmLookup

    Private mProps As LookupProperties
    Private mResult As String

    Private Sub frmLookup_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        GV_SetDefaultStyle(grdView)

        If Not IsNothing(mProps) Then
            ' setup grid
            With grdView
                .AutoGenerateColumns = False
                .DataSource = mProps.DataSet.Tables(0)
                .Columns.Clear()
                Dim xMaxLen As Integer = 10
                For Each oCol As LookupColumn In mProps.Columns
                    xMaxLen = mProps.DataSet.Tables(1).Columns(oCol.FieldName).MaxLength
                    GV_AddColStr(grdView, oCol.Caption, oCol.FieldName, xMaxLen)
                Next
            End With

        End If
    End Sub

    Function ShowLookup(ByVal pProps As LookupProperties) As String
        If IsNothing(pProps) Then
            GoTo FFFFF
        End If

        mProps = pProps
        ShowDialog()

FFFFF:
        Return mResult
    End Function

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        mProps = New LookupProperties
    End Sub

    Private Function ColIndexByField(ByVal pField As String) As Integer
        Dim xRetval As Integer = 0
        For i As Integer = 0 To grdView.Columns.Count - 1
            If grdView.Columns(i).DataPropertyName = pField Then
                xRetval = i
                Exit For
            End If
        Next
        Return xRetval
    End Function

    Private Sub PilihItem()
        Dim xData As String
        xData = grdView.Item(ColIndexByField(mProps.KeyFieldName), grdView.CurrentCell.RowIndex).Value

        mResult = xData
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        mResult = ""
        Me.Close()
    End Sub

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        PilihItem()
    End Sub

    Private Sub grdView_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdView.CellContentClick

    End Sub

    Private Sub grdView_DoubleClick(sender As Object, e As EventArgs) Handles grdView.DoubleClick
        PilihItem()
    End Sub
End Class