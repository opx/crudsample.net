﻿Public Class frmMenu

    Private Sub OpenConn()
        ' open koneksi ke server database
    End Sub

    Private Sub frmMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' open connection

        With DB.ConnParameter
            .Server = "localhost"
            .Port = 9101
            .UserName = "root"
            .Password = "root"
            .Database = "dbcrud"
        End With

        If Not DB.Connect() Then
            MsgBox(DB.LastMsg, MsgBoxStyle.Critical, "Error")
        End If
    End Sub

    Private Sub frmMenu_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        DB = Nothing
    End Sub

    Private Sub KategoriToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KategoriToolStripMenuItem.Click
        Dim oFrm As New frmKategori
        oFrm.MdiParent = Me
        oFrm.Show()
    End Sub

    Private Sub BarangToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BarangToolStripMenuItem.Click
        Dim oFrm As New frmBarang
        oFrm.MdiParent = Me
        oFrm.Show()
    End Sub
End Class
