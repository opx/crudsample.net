﻿Namespace Database

    Public Class DBConnParameter
        Property Server As String
        Property Port As Integer
        Property UserName As String
        Property Password As String
        Property Database As String

        Sub New()
            ' set default value
            Server = ""
            Port = 3306
            UserName = "root"
            Password = ""
            Database = ""
        End Sub
    End Class

End Namespace