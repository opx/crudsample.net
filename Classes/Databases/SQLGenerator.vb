﻿'   SQL Generator
'   Tools to generate INSERT and UPDATE syntax in SQL 
'   Copyright(c) 2019. Noviyanto Wibowo (opx)
'   Compability: MySQL, SQLServer, Hana

Option Explicit On

Namespace Database

    Public Enum enumSQLGeneratorDBType
        edbDefault = 0  ' using standard SQL (MySQL, SQLServer, etc)
        edbHana = 1     ' Hana database
    End Enum

    Public Enum eSQLGeneratorOperation
        egoInsert = 0
        egoUpdate = 1
    End Enum

    Public Enum eSQLGeneratorFieldType
        eftString
        eftDecimal
        eftInteger
        eftUserDefined
    End Enum

    Public Class SQLGeneratorFieldItem
        Public Property FieldName As String
        Public Property FieldType As eSQLGeneratorFieldType
        Public Property Value As String
    End Class

    Public Class SQLGeneratorFieldItems
        Inherits System.Collections.CollectionBase

        Public Sub Add(ByVal Data As SQLGeneratorFieldItem)
            Dim oItem As New SQLGeneratorFieldItem

            With oItem
                .FieldName = Data.FieldName
                .FieldType = Data.FieldType
                .Value = Data.Value
            End With
            List.Add(oItem)
            oItem = Nothing
        End Sub

        Overloads Sub Clear()
            List.Clear()
            InnerList.Clear()
        End Sub
    End Class

    Public Class SQLWhereConditionItem
        Public Property FieldName As String
        Public Property Operand As String
        Public Property Value As VariantType
    End Class

    Public Class SQLWhereConditionItems
        Inherits System.Collections.CollectionBase

        Sub Add(ByVal FieldName As String, ByVal Operand As String, ByVal Value As Object)
            Dim oItem As New SQLWhereConditionItem
            With oItem
                .FieldName = FieldName
                .Operand = Operand
                .Value = Value
            End With
            List.Add(oItem)
            oItem = Nothing
        End Sub

        Overloads Sub clear()
            List.Clear()
            Me.InnerList.Clear()
        End Sub

        Public Function GetQueryString() As String
            Dim oItem As New SQLWhereConditionItem
            Dim SQL As String = ""
            For Each oItem In List
                SQL = SQL & oItem.FieldName & "" & oItem.Operand & "'" & oItem.Value & "'" & " AND "
            Next
            SQL = Left(SQL, Len(SQL) - 5)
            Return SQL
        End Function
    End Class

    Public Class SQLGenerator
        Private mFields As SQLGeneratorFieldItems
        Private FOutputAsNewLine As Boolean
        Private FOutputMinified As Boolean
        Private mDBType As enumSQLGeneratorDBType

        ' Make output SQL display per line or not for each field and value that separated by comma
        Public Property OutputAsNewLine As Boolean
            Get
                Return FOutputAsNewLine
            End Get
            Set(value As Boolean)
                FOutputAsNewLine = value
            End Set
        End Property

        ' Make output minified, OutputAsNewLine automatically disabled
        Public Property OutputMinified As Boolean
            Get
                Return FOutputMinified
            End Get
            Set(value As Boolean)
                FOutputMinified = value
            End Set
        End Property

        ' set output command to standard SQL or HANA
        Public Property DBType As enumSQLGeneratorDBType
            Get
                Return mDBType
            End Get
            Set(value As enumSQLGeneratorDBType)
                mDBType = value
            End Set
        End Property

        Sub New()
            mFields = New SQLGeneratorFieldItems
            FOutputAsNewLine = False
            FOutputMinified = False
            mDBType = enumSQLGeneratorDBType.edbDefault
        End Sub

        ' Varchar/Char field
        Public Sub AddString(ByVal FieldName As String, ByVal Value As String)
            Dim oField = New SQLGeneratorFieldItem
            With oField
                .FieldName = FieldName
                .FieldType = eSQLGeneratorFieldType.eftString
                .Value = Value.ToString
            End With
            mFields.Add(oField)
            oField = Nothing
        End Sub

        ' Float, Double, Decimal field 
        Public Sub AddDecimal(ByVal FieldName As String, ByVal Value As Double)
            Dim oField As New SQLGeneratorFieldItem
            With oField
                .FieldName = FieldName
                .FieldType = eSQLGeneratorFieldType.eftDecimal
                .Value = Value.ToString
            End With
            mFields.Add(oField)
            oField = Nothing
        End Sub

        ' Int field
        Public Sub AddInteger(ByVal FieldName As String, ByVal Value As Integer)
            Dim oField As New SQLGeneratorFieldItem
            With oField
                .FieldName = FieldName
                .FieldType = eSQLGeneratorFieldType.eftInteger
                .Value = Value.ToString
            End With
            mFields.Add(oField)
            oField = Nothing
        End Sub

        ' Date field
        Public Sub AddDate(ByVal FieldName As String, ByVal Value As Date)
            AddString(FieldName, Value.ToString("yyyy-MM-dd"))
        End Sub

        ' DateTime field
        Public Sub AddDateTime(ByVal FieldName As String, ByVal Value As DateTime)
            AddString(FieldName, Value.ToString("yyyy-MM-dd HH:mm:ss"))
        End Sub

        ' Time field
        Public Sub AddTime(ByVal FieldName As String, ByVal Value As DateTime)
            AddString(FieldName, Value.ToString("HH:mm:ss"))
        End Sub

        ' TinyInt(1) field
        Public Sub AddBoolean(ByVal FieldName As String, ByVal Value As Boolean)
            AddInteger(FieldName, IIf(Value, 1, 0))
        End Sub

        ' User Defined Value
        Public Sub AddUDV(ByVal FieldName As String, ByVal Value As String)
            Dim oField As New SQLGeneratorFieldItem
            With oField
                .FieldName = FieldName
                .FieldType = eSQLGeneratorFieldType.eftUserDefined
                .Value = Value
            End With
            mFields.Add(oField)
            oField = Nothing
        End Sub

        ' Parameter
        Public Sub AddParam(ByVal FieldName As String)
            AddUDV(FieldName, "@" & FieldName)
        End Sub

        ' This part will convert SQLGeneratorFieldItem to sql syntax depends on data type..
        ' Numerical (float, decimal, int, tinyint, boolean(tinyint(1))) base value will be convert to FieldName = Value
        ' String base value (date, varchar, char) will be convert to FieldName = 'Value'
        Private Function ToValue(ByVal Data As SQLGeneratorFieldItem) As String
            Dim Retval As String = ""

            Select Case Data.FieldType
                Case eSQLGeneratorFieldType.eftInteger, eSQLGeneratorFieldType.eftDecimal
                    Retval = Data.Value
                Case eSQLGeneratorFieldType.eftString
                    ' handling (') character in SQL
                    Dim xStr As String = Replace(Data.Value, "'", "''")
                    Retval = "'" & xStr & "'"
                Case eSQLGeneratorFieldType.eftUserDefined
                    Retval = Data.Value
            End Select

            Return Retval
        End Function

        Public Sub AssignFieldsFrom(ByVal Data As Object)
            Clear()
            For Each Prop As System.Reflection.PropertyInfo In Data.GetType().GetProperties()
                AddParam(Prop.Name)
            Next
        End Sub

        ' Generator: will parse all stored properties into SQL query
        Public Function Generate(ByVal TableName As String, ByVal GenMethod As eSQLGeneratorOperation, Optional ByVal WhereConditions As SQLWhereConditionItems = Nothing) As String
            Dim SQL As String = ""
            Dim oItem As SQLGeneratorFieldItem
            Dim xField As String = ""
            Dim xValue As String = ""
            Dim xSeparator As String = IIf(FOutputAsNewLine And (Not FOutputMinified), vbCrLf, "")
            Dim xCrLf As String = IIf(FOutputMinified, "", vbCrLf)
            Dim xSpasi As String = IIf(FOutputMinified, " ", "")
            Dim xTable As String = TableName

            If mDBType = enumSQLGeneratorDBType.edbHana Then
                xTable = """" & TableName & """"
            End If

            Select Case GenMethod
                Case eSQLGeneratorOperation.egoInsert
                    For Each oItem In mFields
                        If mDBType = enumSQLGeneratorDBType.edbHana Then
                            xField = xField & """" & oItem.FieldName & """" & "," & xSeparator
                        Else
                            xField = xField & oItem.FieldName & "," & xSeparator
                        End If

                        xValue = xValue & ToValue(oItem) & "," & xSeparator
                    Next
                    xField = xField.Remove(xField.Length() - (xSeparator.Length() + 1))

                    xValue = xValue.Remove(xValue.Length() - (xSeparator.Length() + 1))
                    SQL = "INSERT INTO " & xTable & xCrLf _
                        & "(" & xCrLf _
                        & xField & xCrLf _
                        & ")" & xCrLf _
                        & "VALUES" & xCrLf _
                        & "(" & xCrLf _
                        & xValue & xCrLf _
                        & ")"
                Case eSQLGeneratorOperation.egoUpdate
                    For Each oItem In mFields
                        If mDBType = enumSQLGeneratorDBType.edbHana Then
                            xField = xField & """" & oItem.FieldName & """" & "=" & ToValue(oItem) & "," & xSeparator
                        Else
                            xField = xField & oItem.FieldName & "=" & ToValue(oItem) & "," & xSeparator
                        End If
                    Next
                    xField = xField.Remove(xField.Length() - (xSeparator.Length() + 1))
                    SQL = "UPDATE " & xTable & xSpasi & xCrLf _
                        & "SET " & xCrLf _
                        & xField
            End Select

            If GenMethod = eSQLGeneratorOperation.egoUpdate Then
                If Not IsNothing(WhereConditions) Then
                    SQL = SQL & vbCrLf _
                        & "WHERE " & vbCrLf _
                        & WhereConditions.GetQueryString
                End If
            End If

            Return SQL
        End Function

        Public Sub Clear()
            mFields.Clear()
        End Sub

        Protected Overrides Sub Finalize()
            mFields = Nothing
            MyBase.Finalize()
        End Sub
    End Class

End Namespace