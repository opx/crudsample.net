﻿Imports MySql.Data.MySqlClient

Namespace Database

    Public Class MySQLConn

        Private mConn As MySqlConnection

        Property LastMsg As String
        Property ConnParameter As New DBConnParameter

        Sub New()
            mConn = New MySqlConnection
        End Sub

        ''' <summary>
        ''' Baca object koneksi(MySqlConnection)
        ''' </summary>
        Function Connection() As MySqlConnection
            Return mConn
        End Function

        ''' <summary>
        ''' Konek ke DB
        ''' </summary>
        Function Connect() As Boolean
            Dim oBuilder As New MySqlConnectionStringBuilder
            Dim xConnStr As String = oBuilder.ConnectionString
            Dim xRetval As Boolean = True

            With oBuilder
                .Server = ConnParameter.Server
                .Port = ConnParameter.Port
                .Database = ConnParameter.Database
                .UserID = ConnParameter.UserName
                .Password = ConnParameter.Password
            End With
            xConnStr = oBuilder.ConnectionString
            oBuilder = Nothing

            Try
                mConn.ConnectionString = xConnStr
                mConn.Open()
            Catch ex As Exception
                xRetval = False
                LastMsg = ex.Message
            End Try

            Return xRetval
        End Function

        ''' <summary>
        ''' Open query as DataTable
        ''' </summary>
        Public Function OpenDT(ByVal pQuery As String, Optional ByVal pConn As MySqlConnection = Nothing) As DataTable
            OpenDT = New DataTable

            Dim oCmd As New MySqlCommand
            Dim oAdp As New MySqlDataAdapter
            Dim oDS As New DataSet
            Try
                With oCmd
                    If Not IsNothing(pConn) Then
                        .Connection = pConn
                    Else
                        .Connection = mConn
                    End If
                    .CommandText = pQuery
                End With
                oAdp.SelectCommand = oCmd
                oAdp.Fill(oDS)
                OpenDT = oDS.Tables(0)
            Finally
                oCmd.Dispose()
                oAdp.Dispose()
                oDS.Dispose()
            End Try
        End Function

        ''' <summary>
        ''' Open query as DataSet
        ''' </summary>
        Public Function OpenDS(ByVal pQuery As String, Optional ByVal pConn As MySqlConnection = Nothing,
                                   Optional ByVal pWithSchema As Boolean = False) As DataSet
            OpenDS = New DataSet

            Dim oCmd As New MySqlCommand
            Dim oAdp As New MySqlDataAdapter
            Try
                With oCmd
                    If Not IsNothing(pConn) Then
                        .Connection = pConn
                    Else
                        .Connection = mConn
                    End If
                    .CommandText = pQuery
                End With
                oAdp.SelectCommand = oCmd
                oAdp.Fill(OpenDS)
                If pWithSchema Then
                    oAdp.FillSchema(OpenDS.Tables.Add("SchemaSource"), SchemaType.Source)
                End If
            Finally
                oCmd.Dispose()
                oAdp.Dispose()
            End Try
        End Function

        ''' <summary>
        ''' Eksekusi query
        ''' </summary>
        Public Function Exec(ByVal SQL As String, Optional ByVal Conn As MySqlConnection = Nothing) As Integer
            Dim oCmd As MySqlCommand

            If Not IsNothing(Conn) Then
                oCmd = New MySqlCommand(SQL, Conn)
            Else
                oCmd = New MySqlCommand(SQL, mConn)
            End If
            Return oCmd.ExecuteNonQuery()

            oCmd = Nothing
        End Function

        ''' <summary>
        ''' Cek apakah data ada/tidak di table
        ''' </summary>
        Public Function FindKeyInTables(ByVal pField As String, ByVal pValue As String, ByVal pTables As Array) As Boolean
            Dim Retval As Boolean = False

            If pTables.Length = 0 Then
                GoTo jump
            End If

            Dim SQL As String
            For Each xTable As String In pTables
                SQL = "SELECT COUNT(" + pField + ") AS C " +
                      "FROM   " + xTable + " " +
                      "WHERE  " + pField + " = '" & pValue & "' "

                Dim oDT As DataTable
                oDT = OpenDT(SQL, mConn)
                If oDT.Rows(0)("C") > 0 Then
                    Retval = True
                    Exit For
                End If
            Next

jump:
            Return Retval
        End Function

        ''' <summary>
        ''' Get tanggal & jam server saat ini
        ''' </summary>
        Public Function ServerDateTime() As DateTime
            Dim SQL As String
            Dim xRetval As DateTime
            SQL = "SELECT NOW()"

            Dim oDT As DataTable
            oDT = OpenDT(SQL)
            xRetval = oDT.Rows(0)(0)
            oDT.Dispose()
            Return xRetval
        End Function

        Protected Overrides Sub Finalize()
            mConn.Dispose()
            MyBase.Finalize()
        End Sub

    End Class

End Namespace