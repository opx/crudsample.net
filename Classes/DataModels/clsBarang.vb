﻿Imports CRS.Common.modTipe
Imports CRS.Utility
Imports CRS.Database
Imports MySql.Data.MySqlClient

Namespace DataModel

    Public Class clsBarang
        Private mKode As String = ""

        ' property yang value-nya disimpan di variabel lokal
        Property KodeBarang As String
            Get
                Return mKode
            End Get
            Set(value As String)
                mKode = value
            End Set
        End Property

        ' property yang value-nya diset public
        Property NamaBarang As String
        Property KodeKategori As String
        Property Aktif As Boolean
        Property IsExists As Boolean
        Property LastMsg As String ' berisi pesan error dsb

        ''' <summary>
        ''' saat init object, kosongkan semua property
        ''' </summary>
        Public Sub New()
            Reset()
        End Sub

        ''' <summary>
        ''' kosongkan property sesuai tipe datanya daripada diset "nothing/null"
        ''' </summary>
        Sub Reset()
            NamaBarang = ""
            KodeKategori = ""
            Aktif = False
            IsExists = False
            LastMsg = ""
        End Sub

        ''' <summary>
        ''' baca data dan isi semua property
        ''' </summary>
        Sub GetData(ByVal pKode As String)
            mKode = pKode
            Reset()

            ' jika parameter kosong maka abaikan
            If Trim(mKode) = "" Then
                Exit Sub
            End If

            Dim sSQL As String
            sSQL = " SELECT     KodeBarang, NamaBarang, KodeKategori, Aktif " & vbCrLf _
                 & " FROM       barang " & vbCrLf _
                 & " WHERE      KodeBarang = '" & pKode & "' "
            Dim oDT As DataTable = DB.OpenDT(sSQL)
            If oDT.Rows.Count > 0 Then
                mKode = CStr(oDT.Rows(0)("KodeBarang"))
                NamaBarang = CStr(oDT.Rows(0)("NamaBarang"))
                KodeKategori = CStr(oDT.Rows(0)("KodeKategori"))
                Aktif = CBool(oDT.Rows(0)("Aktif"))
                IsExists = True
            End If
            oDT.Dispose()
        End Sub

        ''' <summary>
        ''' Simpan data
        ''' </summary>
        Function Save(ByVal pSaveMode As enumSaveMode) As Boolean
            Dim xLanjut As Boolean = True

            '1. Set semua validasi
            If xLanjut Then
                ' jika data baru
                If pSaveMode = enumSaveMode.esmNew Then
                    If Trim(KodeBarang) = "" Then
                        xLanjut = False
                        LastMsg = "Kode Barang kosong"
                    End If
                Else
                    ' cari data ada / tidak
                    If Not DB.FindKeyInTables("KodeBarang", KodeBarang, {"barang"}) Then
                        xLanjut = False
                        LastMsg = "Data tidak ditemukan"
                    End If
                End If
            End If

            If xLanjut Then
                If Trim(NamaBarang) = "" Then
                    xLanjut = False
                    LastMsg = "Nama Barang kosong"
                End If
            End If

            If xLanjut Then
                If Trim(KodeKategori) = "" Then
                    xLanjut = False
                    LastMsg = "Kategori kosong"
                Else
                    Dim oKat As New clsKategori
                    oKat.GetData(KodeKategori)
                    If Not oKat.IsExists Then
                        xLanjut = False
                        LastMsg = "Kategori invalid"
                    End If
                    oKat = Nothing
                End If
            End If

            ' 2. Proses simpan
            If xLanjut Then
                Dim oTrans As MySqlTransaction = DB.Connection.BeginTransaction
                Dim oGen As New SQLGenerator
                Try
                    Dim sSQL As String
                    With oGen
                        .AddString("KodeBarang", KodeBarang)
                        .AddString("NamaBarang", NamaBarang)
                        .AddString("KodeKategori", KodeKategori)
                        .AddBoolean("Aktif", Aktif)
                        If pSaveMode = enumSaveMode.esmNew Then
                            sSQL = .Generate("barang", eSQLGeneratorOperation.egoInsert)
                        Else
                            sSQL = .Generate("barang", eSQLGeneratorOperation.egoUpdate) & vbCrLf _
                                 & " WHERE KodeBarang = '" & KodeBarang & "' "
                        End If
                    End With
                    DB.Exec(sSQL)

                    oTrans.Commit()
                Catch ex As Exception
                    If Not IsNothing(oTrans) Then
                        oTrans.Rollback()
                    End If
                    xLanjut = False
                    LastMsg = ex.Message
                Finally
                    If Not IsNothing(oTrans) Then
                        oTrans.Dispose()
                    End If
                    oGen = Nothing
                End Try
            End If

            Return xLanjut
        End Function


        ''' <summary>
        ''' Hapus data
        ''' </summary>
        Function Delete(Optional ByVal pKode As String = "") As Boolean
            Dim xLanjut As Boolean = True

            ' jika parameter pKode terisi maka key value ambil dari parameter 
            ' dan jika tidak terisi maka key value ambil dari property Kode
            Dim xKey As String = IIf(Trim(pKode) <> "", pKode, KodeBarang)

            ' 1. Set validasi
            If xLanjut Then
                If Trim(xKey) = "" Then
                    xLanjut = False
                    LastMsg = "Kode Barang kosong"
                End If
            End If

            If xLanjut Then
                Dim oData As New clsBarang
                oData.GetData(xKey)
                If Not oData.IsExists Then
                    xLanjut = False
                    LastMsg = "Data tidak ditemukan"
                End If
                oData = Nothing
            End If

            ' 2. Proses Delete
            If xLanjut Then
                Dim oTrans As MySqlTransaction = DB.Connection.BeginTransaction
                Try
                    Dim sSQL As String
                    sSQL = " DELETE FROM barang WHERE KodeBarang = '" & xKey & "' "

                    DB.Exec(sSQL)
                    oTrans.Commit()
                Catch ex As Exception
                    If Not IsNothing(oTrans) Then
                        oTrans.Rollback()
                    End If
                    xLanjut = False
                    LastMsg = ex.Message
                Finally
                    If Not IsNothing(oTrans) Then
                        oTrans.Dispose()
                    End If
                End Try
            End If

            Return xLanjut
        End Function

        Public Function LookupAll() As String
            Dim sSQL As String
            sSQL = " SELECT     aa.KodeBarang, aa.NamaBarang, bb.NamaKategori " & vbCrLf _
                 & " FROM       barang aa " & vbCrLf _
                 & " LEFT JOIN  kategori bb ON aa.KodeKategori = bb.KodeKategori " & vbCrLf _
                 & " ORDER BY   NamaBarang "
            Dim oDS As DataSet = DB.OpenDS(sSQL, , True)

            Dim oLP As New LookupProperties
            oLP.DataSet = oDS
            oLP.Columns.Add("Kode", "KodeBarang")
            oLP.Columns.Add("Nama", "NamaBarang")
            oLP.Columns.Add("Kategori", "NamaKategori")
            oLP.KeyFieldName = "KodeKategori"

            Dim oLookup As New LookupForm
            Return oLookup.Show(oLP)
        End Function

    End Class

End Namespace