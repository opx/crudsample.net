﻿Imports CRS.Common.modTipe
Imports CRS.Utility
Imports CRS.Database
Imports MySql.Data.MySqlClient

Namespace DataModel

    Public Class clsKategori
        Private mKode As String = ""

        ' property yang value-nya disimpan di variabel lokal
        Property KodeKategori As String
            Get
                Return mKode
            End Get
            Set(value As String)
                mKode = value
            End Set
        End Property

        ' property yang value-nya diset public
        Property NamaKategori As String
        Property Aktif As Boolean
        Property IsExists As Boolean
        Property LastMsg As String ' berisi pesan error dsb

        ''' <summary>
        ''' saat init object, kosongkan semua property
        ''' </summary>
        Public Sub New()
            Reset()
        End Sub

        ''' <summary>
        ''' kosongkan property sesuai tipe datanya daripada diset "nothing/null"
        ''' </summary>
        Sub Reset()
            NamaKategori = ""
            Aktif = False
            IsExists = False
            LastMsg = ""
        End Sub

        ''' <summary>
        ''' baca data dan isi semua property
        ''' </summary>
        Sub GetData(ByVal pKode As String)
            mKode = pKode
            Reset()

            ' jika parameter kosong maka abaikan
            If Trim(mKode) = "" Then
                Exit Sub
            End If

            Dim sSQL As String
            sSQL = " SELECT     KodeKategori, NamaKategori, Aktif " & vbCrLf _
                 & " FROM       kategori " & vbCrLf _
                 & " WHERE      KodeKategori = '" & pKode & "' "
            Dim oDT As DataTable = DB.OpenDT(sSQL)
            If oDT.Rows.Count > 0 Then
                mKode = CStr(oDT.Rows(0)("KodeKategori"))
                NamaKategori = CStr(oDT.Rows(0)("NamaKategori"))
                Aktif = CBool(oDT.Rows(0)("Aktif"))
                IsExists = True
            End If
            oDT.Dispose()
        End Sub

        ''' <summary>
        ''' Simpan data
        ''' </summary>
        Function Save(ByVal pSaveMode As enumSaveMode) As Boolean
            Dim xLanjut As Boolean = True

            '1. Set semua validasi
            If xLanjut Then
                ' jika data baru
                If pSaveMode = enumSaveMode.esmNew Then
                    If Trim(KodeKategori) = "" Then
                        xLanjut = False
                        LastMsg = "Kode Kategori kosong"
                    End If
                Else
                    ' cari data ada / tidak
                    If Not DB.FindKeyInTables("KodeKategori", KodeKategori, {"kategori"}) Then
                        xLanjut = False
                        LastMsg = "Data tidak ditemukan"
                    End If
                End If
            End If

            If xLanjut Then
                If Trim(NamaKategori) = "" Then
                    xLanjut = False
                    LastMsg = "Nama Kategori kosong"
                End If
            End If

            ' 2. Proses simpan
            If xLanjut Then
                Dim oTrans As MySqlTransaction = DB.Connection.BeginTransaction
                Dim oGen As New SQLGenerator
                Try
                    Dim sSQL As String
                    With oGen
                        .AddString("KodeKategori", KodeKategori)
                        .AddString("NamaKategori", NamaKategori)
                        .AddBoolean("Aktif", Aktif)
                        If pSaveMode = enumSaveMode.esmNew Then
                            sSQL = .Generate("kategori", eSQLGeneratorOperation.egoInsert)
                        Else
                            sSQL = .Generate("kategori", eSQLGeneratorOperation.egoUpdate) & vbCrLf _
                                 & " WHERE KodeKategori = '" & KodeKategori & "' "
                        End If
                    End With
                    DB.Exec(sSQL)

                    oTrans.Commit()
                Catch ex As Exception
                    If Not IsNothing(oTrans) Then
                        oTrans.Rollback()
                    End If
                    xLanjut = False
                    LastMsg = ex.Message
                Finally
                    If Not IsNothing(oTrans) Then
                        oTrans.Dispose()
                    End If
                    oGen = Nothing
                End Try
            End If

            Return xLanjut
        End Function


        ''' <summary>
        ''' Hapus data
        ''' </summary>
        Function Delete(Optional ByVal pKode As String = "") As Boolean
            Dim xLanjut As Boolean = True

            ' jika parameter pKode terisi maka key value ambil dari parameter 
            ' dan jika tidak terisi maka key value ambil dari property Kode
            Dim xKey As String = IIf(Trim(pKode) <> "", pKode, KodeKategori)

            ' 1. Set validasi
            If xLanjut Then
                If Trim(xKey) = "" Then
                    xLanjut = False
                    LastMsg = "Kode Kategori kosong"
                End If
            End If

            If xLanjut Then
                Dim oData As New clsKategori
                oData.GetData(xKey)
                If Not oData.IsExists Then
                    xLanjut = False
                    LastMsg = "Data tidak ditemukan"
                End If
                oData = Nothing
            End If

            ' 2. Proses Delete
            If xLanjut Then
                Dim oTrans As MySqlTransaction = DB.Connection.BeginTransaction
                Try
                    Dim sSQL As String
                    sSQL = " DELETE FROM kategori WHERE KodeKategori = '" & xKey & "' "

                    DB.Exec(sSQL)
                    oTrans.Commit()
                Catch ex As Exception
                    If Not IsNothing(oTrans) Then
                        oTrans.Rollback()
                    End If
                    xLanjut = False
                    LastMsg = ex.Message
                Finally
                    If Not IsNothing(oTrans) Then
                        oTrans.Dispose()
                    End If
                End Try
            End If

            Return xLanjut
        End Function

        ''' <summary>
        ''' List data untuk kategori aktif
        ''' </summary>
        ''' <returns>DataTable</returns> 
        Public Function DT_ListAktif() As DataTable
            Dim sSQL As String
            sSQL = " SELECT     KodeKategori, NamaKategori " & vbCrLf _
                 & " FROM       kategori " & vbCrLf _
                 & " WHERE      Aktif = 1 "
            Return DB.OpenDT(sSQL)
        End Function

        ''' <summary>
        ''' Lookup all data
        ''' </summary>
        Public Function LookupAll() As String
            Dim sSQL As String
            sSQL = " SELECT     KodeKategori, NamaKategori " & vbCrLf _
                 & " FROM       kategori " & vbCrLf _
                 & " ORDER BY   NamaKategori "
            Dim oDS As DataSet = DB.OpenDS(sSQL, , True)

            Dim oLP As New LookupProperties
            oLP.DataSet = oDS
            oLP.Columns.Add("Kode", "KodeKategori")
            oLP.Columns.Add("Kategori", "NamaKategori")
            oLP.KeyFieldName = "KodeKategori"

            Dim oLookup As New LookupForm
            Return oLookup.Show(oLP)
        End Function

    End Class

End Namespace