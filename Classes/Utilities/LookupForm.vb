﻿Namespace Utility

    Public Class LookupColumn
        Property Caption As String
        Property FieldName As String
        Property Width As Integer
    End Class

    Public Class LookupColumns
        Inherits System.Collections.CollectionBase

        Sub Add(ByVal pCaption As String, ByVal pFieldName As String, Optional ByVal pWidth As Integer = 0)
            Dim oItem As New LookupColumn
            With oItem
                .Caption = pCaption
                .FieldName = pFieldName
                .Width = pWidth
            End With
            Me.List.Add(oItem)
            oItem = Nothing
        End Sub
    End Class

    Public Class LookupProperties
        Property DataSet As DataSet
        Property Columns As LookupColumns
        Property KeyFieldName As String

        Sub New()
            Columns = New LookupColumns
        End Sub
    End Class

    Public Class LookupForm

        Function Show(ByVal pProps As Object) As String
            Dim oFrm As New frmLookup
            Return oFrm.ShowLookup(pProps)
        End Function

    End Class

End Namespace